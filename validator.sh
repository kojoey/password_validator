#!/bin/bash

PASSWORD=$1
RED="\e[31m\e[1m" # add at the beginning of a string to change the output to the color red.
GREEN="\e[32m\e[1m" # add at the beginning of a string to change the output to the color green.
GLOWING_GREEN="\e[42m\e[1m" # add at the beginning of a string to add a green marker sign to the output.
ENDING="\e[0m" # used to end any change to the output color of a string.
LEN="${#PASSWORD}"
if test "$LEN" -ge 9 ; then # Checks the length of the password.
    echo "$PASSWORD" | grep -q [0-9] # Checks for an integer in the password.
    if test $? -eq 0 ; then
           echo "$PASSWORD" | grep -q [A-Z] # Checks if the password contains a uppercase letter
                if test $? -eq 0 ; then
                    echo "$PASSWORD" | grep -q [a-z] # Checks if the password contains a lowercase letter
                      if test $? -eq 0 ; then
                       echo -e "${GREEN}Strong password, ${ENDING}${GLOWING_GREEN}Well done champion!${ENDING}" ;exit 0;
                   else
                       echo -e "${RED}WEAK password,please include a lower case letter.${ENDING}";exit 1;
                   fi
            else
               echo -e "${RED}WEAK password,please include a capital case letter.${ENDING}";exit 1;
            fi
     else
       echo -e "${RED}WEAK password,please include an integer.${ENDING}";exit 1;
     fi
else
    echo -e "${RED}WEAK password, password length should be greater than or equal to 10.${ENDING}";exit 1;
fi