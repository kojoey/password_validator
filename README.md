
# **Password Validator**
## Bash and Powershell

This my password Validator script, made in both Bash and Powershell.  <br />
Feature branch in both(Bash and Powershell) adds the ability to pass a file as a variable to the script and fetch the password out of that file ( The file must contain only the password ) <br />

Usage :  <br />
`./password-validator.sh "MyP@ssw0rd!"`  <br />
`./password-validator.sh -f "/mypath/password.txt"`  <br />

# **Bonus Branch**

The bonus branch contains a script that open notepad with the text "Hello Bootcamp!" (The file will be created if one isn't present already". <br />
It also contains another script that creates a Scheduled Windows Task to run the "Open_Notepad" script,  <br />
This script accepts 3 (Mandatory) parameters :  <br />
**TaskName** - The name/tag you want to give to the task  <br />
**TriggerSeconds** - Time to wait between each execution of the task  <br />
**WaitSeconds** - Time to wait untill said that is disabled  <br />
### ScreenShot to Task created <br />
(https://pasteboard.co/piM3UxgjD30v.png)
<br />
  
Usage :  <br />
` ./task.ps1 -TaskName "MyName" -TriggerSeconds “60” -WaitSeconds "120”`
